const setCharges = charges => ({
  type: 'LOAD_CHARGES',
  charges,
});


export const loadCharges = (
  limit = 5,
  page = 0
) =>
  (dispatch, _, api) => {
    api.getCharges(limit, page)
      .then(charges => dispatch(setCharges(charges)))
      .catch(err => console.log(err))
  }
