import React, { Component } from 'react';
import { Router , Route, IndexRoute, hashHistory } from 'react-router';

import Charges from './Charges';
import Details from './Details';

const Main = ({children}) => children;

class App extends Component {
  render() {
    return (
      <Router history={hashHistory}>
        <Route path="/" component={Main}>
          <IndexRoute component={Charges} />
          <Route path="/detail/:id" component={Details} />
        </Route>
      </Router>
    );
  }
}

export default App;
