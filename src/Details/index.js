import React, { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';

import * as formatters from '../formatters';
import Section from '../common/Section';
import Table from '../common/Table';

const sectionTx = [{
  label: 'num pedido',
  path: 'id',
}, {
  label: 'descripcion',
  path: 'product_description',
}, {
  label: 'fecha y hora',
  path: 'date',
  format: formatters.dateTime,
}];

const sectionCustomer = [{
  label: 'Pais',
  path: 'client.country_code',
}, {
  label: 'Ciudad',
  path: 'client.address_city',
}, {
  label: 'Direccion',
  path: 'client.address',
}, {
  label: 'Email',
  path: 'client.email',
}, {
  label: 'Telefono',
  path: 'client.phone',
}];

const sectionCard = [{
  label: 'Nombre completo',
  path: ['token.cardholder.first_name', 'token.cardholder.last_name'],
}, {
  label: 'email',
  path: 'token.cardholder.email',
}, {
  label: 'Banco',
  path: 'token.brand_name',
}, {
  label: 'Origin del banco',
  path: 'token.bank_country',
}];

const mapFields = (item, fields) =>
  fields.map(({label, path, format}) => ({
    label: label,
    value: formatters.extractValue(item, path, format)
  }));

const currencies = {
  PEN: 'soles',
};

const feesHeaders = [{
  label: 'tipo',
  path: 'type'
}, {
  label: 'Moneda',
  path: 'currency_code',
  format: val => currencies[val]
}, {
  label: 'Monto',
  path: 'amount'
}, {
  label: 'Impuesto',
  path: 'taxes'
}, {
  label: 'Total',
  path: 'total_amount'
}];

class Details extends Component {
  _onClick = () => {
    this.props.router.push('/');
  }

  render() {
    const {
      charge,
    } = this.props;

    return (
      <div>
        <RaisedButton label="Back" primary onClick={this._onClick}/>
        <h1>Ventas {formatters.amount(charge.current_amount)}</h1>
        <h2>{charge.merchant_message}</h2>
        <Section
          title={'Datos de la transaccion'}
          fields={mapFields(charge, sectionTx)}
        />
        <Section
          title={'Datos del cliente'}
          fields={mapFields(charge, sectionCustomer)}
        />
        <Section
         title={'Datos de la tarejta'}
         fields={mapFields(charge, sectionCard)}
        />
        <Section title={'Impuestos'}>
          <Table
            headers={feesHeaders}
            data={charge.fees}
          />
        </Section>
      </div>
    );
  }
};

const mapStateToProps = ({ charges }, props) => ({
  charge: charges.filter(charge => charge.id === +props.params.id)[0],
});

export default connect(mapStateToProps)(Details);
