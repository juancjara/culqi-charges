import moment from 'moment';

export const amount = val => `S/. ${(val/100).toFixed(2)}`;

export const dateTime = val => moment(+val).format('MM/DD/YYYY HH:mm:ss');

export const extractValue = (item, rawPaths, format = val => val) => {
  return format([].concat(rawPaths)
    .map(rawPath => rawPath.split('.').reduce((tempVal, path) => tempVal[path], item))
     .join(' '));
};
