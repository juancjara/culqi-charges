import React from 'react';
import { Circle } from 'react-progressbar.js';
import interpolate from 'color-interpolate';

const colormap = interpolate(['red', 'yellow', 'green']);

const getOptions = value => ({
  strokeWidth: 4,
  color: colormap(value/100),
});

const FraudCell = ({ value }) => (
  <div style={{width:50, height: 50, padding: 10}}>
    <Circle
      progress={value}
      options={getOptions(value)}
      text={`${Math.round(value)} %`}
      contentStyle={{width: '50px', height: '50px'}}
    />
  </div>
);

export default FraudCell;
