import React, { Component } from 'react';
import { connect } from 'react-redux';

import Table from '../common/Table';
import Fraud from './Components/FraudCell';

import { loadCharges } from '../actions';
import * as formatters from '../formatters';

const columns = [{
  path: 'object',
  label: '',
}, {
  path: 'current_amount',
  label: 'Monto',
  style: {fontWeight: 'bold'},
  format: formatters.amount,
}, {
  path: 'reference_code',
  label: 'Cod ref',
}, {
  path: ['token.cardholder.first_name', 'token.cardholder.last_name'],
  label: 'Cliente',
}, {
  path: 'token.brand_name',
  label: 'Marca',
  style: {fontWeight: 'bold'}
}, {
  path: 'token.card_number',
  label: 'Tarjeta',
}, {
  path: 'state',
  label: 'Estado',
}, {
  path: 'date',
  label: 'Fecha y hora',
  format: formatters.dateTime,
}, {
  path: 'fraud_score',
  label: 'Fraude',
  component: Fraud,
}];


class Charge extends Component {

  loadCharges = (limit = 10, page = 0) => {
    this.props.loadCharges(limit, page);
  }

  componentDidMount(){
    this.loadCharges();
  }
  _onCellClick = (row) => {
    this.props.router.push('/detail/' + this.props.charges[row].id);
  }

  render() {
    const {
      charges,
    } = this.props;

    return (
      <Table
        headers={columns}
        data={charges}
        onCellClick={this._onCellClick}
      />
    );
  }
}

const mapStateToProps = ({charges}) => ({ charges});
const mapDispatchToProps = (dispatch) => ({
  loadCharges: (limit, page) => dispatch(loadCharges(limit, page)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Charge);
