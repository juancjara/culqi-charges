import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import * as api from './api';

const initialState = {
  charges: []
};

const rootReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'LOAD_CHARGES':
      return {
        ...state,
        charges: action.charges,
      };
    default:
      return state;
  }
};


export default () =>
  createStore(
    rootReducer,
    applyMiddleware(thunk.withExtraArgument(api))
  )
