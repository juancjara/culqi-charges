import React from 'react';

import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';


import Headers from './Headers';
import * as formatters from '../formatters';


const renderCell = (item, {component, format, path: rawPaths}) => {
  const value = formatters.extractValue(item, rawPaths, format);
  if (component) {
    return React.createElement(component, {value})
  }
  return value;
};


const TableWrapper =({
  headers,
  data,
  onCellClick = () => {}
}) => (
  <Table
    selectable={false}
    onCellClick={onCellClick}
  >
    <TableHeader displaySelectAll={false}>
      <Headers headers={headers} />
    </TableHeader>
    <TableBody stripedRows={true} displayRowCheckbox={false}>
    {
      data.map((item, i) =>
        <TableRow key={i} onClick={() => console.log(i)} >
          {headers.map((header, j) =>
            <TableRowColumn key={i+''+j} style={header.style}>
              {renderCell(item, header)}
            </TableRowColumn>
          )}
        </TableRow>
      )}
    </TableBody>
  </Table>
);

export default TableWrapper;
