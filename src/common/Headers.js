import React from 'react';
import {
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table';

const ChargeTableHeader = ({
  headers
}) => (
    <TableRow>
    {
      headers.map(({ label }, i) =>
       <TableHeaderColumn key={i}>{label}</TableHeaderColumn>)
    }
  </TableRow>
);

export default ChargeTableHeader;
