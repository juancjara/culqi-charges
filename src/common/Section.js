import React from 'react';

const Section = ({
  title,
  fields = [],
  children,
}) => (
    <div>
    <h3>{title}</h3>
    {
      fields.map(({label, value}, i) =>
                 <div key={i}>
                 <label>{label}: </label>
                 <span>{value}</span>
                 </div>
      )
    }
    {children}
  </div>
);

export default Section;
